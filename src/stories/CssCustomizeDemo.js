import React, { Component } from 'react';

import Select from '../components/Select';

const style = {
    display: 'flex',
    justifyContent: 'center',
    alignItems: 'center',
    width: '100vw',
    height: '100vh',
};
const selectStyle = {
    width: '400px',
    heigth: '50px',
    boxShadow:
        '0px 5px 5px -3px rgba(0, 0, 0, 0.2), 0px 8px 10px 1px rgba(0, 0, 0, 0.14), 0px 3px 14px 2px rgba(0, 0, 0, 0.12)',
    backgroundColor: '#fff',
    borderRadius: '5px',
    paddingLeft: '10px',
};

const imageStyle = {
    width: '40px',
    height: '40px',
};

const Image = () => (
    <img
        src="https://maxcdn.icons8.com/Android_L/PNG/96/Military/tank-96.png"
        style={imageStyle}
        alt="logo"
    />
);

export default class DefaultDemo extends Component {
    state = { value: null };
    onChosesItem = item => {
        this.setState({ value: item });
    };
    render() {
        const { items } = this.props;
        const { value } = this.state;

        return (
            <div style={style}>
                <Select
                    style={selectStyle}
                    value={value}
                    items={items}
                    onChange={this.onChosesItem}
                    iconComponent={Image}
                />
            </div>
        );
    }
}
