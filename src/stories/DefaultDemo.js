import React, { Component } from 'react';

import Select from '../components/Select';

const style = {
    display: 'flex',
    justifyContent: 'center',
    alignItems: 'center',
    width: '100vw',
    height: '100vh',
};

export default class DefaultDemo extends Component {
    state = { value: null };
    onChosesItem = item => {
        this.setState({ value: item });
    };
    render() {
        const { items } = this.props;
        const { value } = this.state;

        return (
            <div style={style}>
                <Select value={value} items={items} onChange={this.onChosesItem} />
            </div>
        );
    }
}
