import React, { Component } from 'react';

import Select from '../components/Select';

const style = {
    display: 'flex',
    justifyContent: 'center',
    alignItems: 'center',
    width: '100vw',
    height: '100vh',
};

export default class MultipleDemo extends Component {
    state = { value: null };
    onChosesItem = items => {
        const value = items.join(', ').slice(0, 20);
        this.setState({ value: value === items.join(', ') ? value : `${value}...` });
    };
    render() {
        const { items } = this.props;
        const { value } = this.state;

        return (
            <div style={style}>
                <Select value={value} multiple items={items} onChange={this.onChosesItem} />
            </div>
        );
    }
}
